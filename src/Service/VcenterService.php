<?php 
namespace Plusai\Vcenter\Service;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class VcenterService
{
    private $token = null;

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function login($username,$password)
    {
        $credentials = base64_encode($username . ':' . $password);
        $endpoint = config('vcenter.ip') . '/rest/com/vmware/cis/session';
        $response = Http::withOptions(['verify' => false])
        ->withHeaders([
            'Authorization' => 'Basic ' . $credentials
        ])->post($endpoint);

        $arrayResponse = json_decode($response->body());
        return  $arrayResponse->value;
    }
    


    public function cloneTemplate($libraryItemId,$name,$folder,$cluster,$cpu,$ram,$disk_id,$disk_size,$custom_spec_name)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));
        Log::info($this->token);
        $endpoint = config('vcenter.ip') . '/api/vcenter/vm-template/library-items/';
        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint . $libraryItemId . '?action=deploy',[
            'name' => $name,
            'placement' => [
                'folder' => $folder,
                'cluster' => $cluster
            ],
            'hardware_customization' => [
                'cpu_update' => [
                    'num_cores_per_socket' => 1,
                    'num_cpus' => $cpu,
                ],
                'disks_to_update' => [
                    $disk_id => [
                        'capacity' => $disk_size * 1073741824 + 1048579
                    ]
                ],
                'memory_update' => [
                    'memory' => $ram * 1024
                ]
            ],
            "guest_customization" => [
                "name" => $custom_spec_name,
            ]

        ]);

        
        $arrayResponse = json_decode($response->body());

    
        return $arrayResponse;
    }

    public function getPowerState(int $vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';
        
        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->get($endpoint . 'vm-' . $vmId . '/power');

        $arrayResponse = json_decode($response->body());
        return $arrayResponse->state;
    }

    public function on(int $vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint . 'vm-' . $vmId . '/power?action=start');

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function off(int $vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint . 'vm-' . $vmId . '/power?action=stop');

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function reset(int $vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint . 'vm-' . $vmId . '/power?action=reset');

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function reboot(int $vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint . 'vm-' . $vmId . '/guest/power?action=reset');

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function delete(int $vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->delete($endpoint . 'vm-' . $vmId);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function console(int $vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint . 'vm-' . $vmId . '/console/tickets',['type' => 'WEBMKS']);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function updateRam(int $vmId,int $ramSize,bool $hotplug = false)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->patch($endpoint . 'vm-' . $vmId . '/hardware/memory',[
            'hot_add_enabled' => true,
            'size_MiB' => $ramSize
        ]);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function updateCpu(int $vmId,int $cores,int $count,bool $hotAdd = false,bool $hotRemove = false)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->patch($endpoint . 'vm-' . $vmId . '/hardware/cpu',[
            'cores_per_socket' => 1,
            'count' => $count,
            'hot_add_enabled' => $hotAdd,
            'hot_remove_enabled' => $hotRemove,
        ]);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function createCustomSpec($specName,$specDesc,Array $osConfig,$dns1,$dns2,$gateway,$ip)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

       
        $endpoint = config('vcenter.ip') . '/api/vcenter/guest/customization-specs';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint,[
                "name" => $specName,
                "description" => $specDesc,
                "spec" => [
                    "configuration_spec" => $osConfig,
                    "global_DNS_settings" => [
                        "dns_servers" => [
                            $dns1,
                            $dns2
                        ]
                    ],
                    "interfaces" => [
                        [
                            "adapter" => [
                                "ipv4" => [
                                    "gateways" => [
                                        $gateway
                                    ],
                                    "ip_address" => $ip,
                                    "prefix" => 24,
                                    "type" => "STATIC"
                                ],
                                "windows" => [
                                    "dns_servers" => [
                                        $dns1,
                                        $dns2
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
        ]);
        
        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function connectToEthernet($vmId,$nicId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/vm-' . $vmId . '/hardware/ethernet/'. $nicId .'?action=connect';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->post($endpoint);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
        
    }

    public function getDisks($vmId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/vm-' . $vmId . '/hardware/disk';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->get($endpoint);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function getDiskinfo($vmId,$diskId)
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/vm-' . $vmId . '/hardware/disk/' . $diskId;

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->get($endpoint);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }

    public function getVmList()
    {
        // $this->login(config('vcenter.username'),config('vcenter.password'));

        $endpoint = config('vcenter.ip') . '/api/vcenter/vm/';

        $response = Http::withOptions(['verify' => false])
        ->withHeaders(['cookie' => 'vmware-api-session-id=' . $this->token])
        ->get($endpoint);

        $arrayResponse = json_decode($response->body());
        return $arrayResponse;
    }
    
}


?>
<?php

namespace Plusai\Vcenter;

use Illuminate\Support\ServiceProvider;
use Plusai\Vcenter\Service\VcenterService;
use Plusai\Vcenter\Vm\Vm;

class VcenterServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('vcenter.php'),
        ],'vcenter');
        
        $venterService = new VcenterService;
        $this->app->bind('vm',function() use($venterService){
            return new Vm($venterService);
        });
    }


    public function register()
    {

    }
}

?>
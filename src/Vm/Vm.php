<?php 
namespace Plusai\Vcenter\Vm;

use Plusai\Vcenter\Service\VcenterService;

class Vm 
{
    private $vcenterService;

    public function __construct(vcenterService $vcenterService)
    {
        $this->vcenterService = $vcenterService;
    }

    public function setToken($token)
    {
        $this->vcenterService->setToken($token);
    }


    public function login($username,$password)
    {
        return $this->vcenterService->login($username,$password);
    }

    public function cloneTemplate($libraryItemId,$name,$folder,$cluster,$cpu,$ram,$disk_id,$disk_size,$custom_spec_name)
    {
        return $this->vcenterService->cloneTemplate($libraryItemId,$name,$folder,$cluster,$cpu,$ram,$disk_id,$disk_size,$custom_spec_name);
    }

    public function getPowerState(int $vmId)
    {
        return $this->vcenterService->getPowerState($vmId);
    }

    public function on(int $vmId)
    {
        return $this->vcenterService->on($vmId);
    }

    public function off(int $vmId)
    {
        return $this->vcenterService->off($vmId);
    }

    public function reset(int $vmId)
    {
        return $this->vcenterService->reset($vmId);
    }

    public function reboot(int $vmId)
    {
        return $this->vcenterService->reset($vmId);
    }

    public function delete(int $vmId)
    {
        return $this->vcenterService->delete($vmId);
    }

    public function console(int $vmId)
    {
        return $this->vcenterService->console($vmId);
    }

    public function updateRam(int $vmId,int $ramSize,bool $hotplug = false)
    {
        return $this->vcenterService->updateRam($vmId,$ramSize,$hotplug);
    }

    public function updateCpu(int $vmId,int $cores,int $counts,bool $hotadd = false,bool $hotremove = false)
    {
        return $this->vcenterService->updateCpu($vmId,$cores,$counts,$hotadd,$hotremove);
    }

    public function createCustomSpec($specName,$specDesc,Array $osConfig,$dns1,$dns2,$gateway,$ip)
    {
        return $this->vcenterService->createCustomSpec($specName,$specDesc,$osConfig,$dns1,$dns2,$gateway,$ip);
    }

    public function connectToEthernet(int $vmId,int $nicId)
    {
        return $this->vcenterService->connectToEthernet($vmId,$nicId);
    }

    public function getDisks(int $vmId)
    {
        return $this->vcenterService->getDisks($vmId);
    }

    public function getDiskinfo(int $vmId,int $diskId)
    {
        return $this->vcenterService->getDiskinfo($vmId,$diskId);
    }

    public function getVmList()
    {
        return $this->vcenterService->getVmList();
    }
    
}


?>
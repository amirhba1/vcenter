<?php 
namespace Plusai\Vcenter\Vm;

use Illuminate\Support\Facades\Facade;

class VmFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'vm';
    }
}


?>